# Basic Questions

## 1. How important is logging in an application? 

Logging in any application is very important, you can log a lot of critical things and gain unprecedented insight into how your application is working and performing. Logs are the medium through which your app speaks to you and it is the foundation for solving your operational problems so not having them is being in a communication void with your application.

## 2. When do you think use of global variables is appropriate?

When you would like to share data inside the application. It very easy to pass and access data from other classes and methods, you do not need to pass variable every time you call a method or class. Finally, using global variable help to minimize local variable creation and less memory usage.

## 3. Name 3 DO’s and 3 DON’Ts around Exception Handling

DO

    + You should specific information in your logs that helps you know the root of an error.
 	+ Do inherit from the Exception class for custom exceptions. You can define your own exceptions for specific scenarios such as define a specific error for your data layers to catch connection issues.
 	+ Create try-catch for specific code.


DON'T

    + Don't have an empty catch statement. You should log the error so you can know what error when something goes wrong.
 	+ Don't throw errors if that are not errors. For example, when you query a database for a list of items and no items are returned, this is not error.
 	+ Don't just wrap entire method with only one try-catch.

 	