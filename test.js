
// Quadratic-Time O(N 2)
function removeDuplicate(array){
    var outputArray = [];
    var duplicate = false;

    for(let n = 0; n < array.length; n ++){ //array.length interations
        for(let m = 0; m < outputArray.length; m++){ // (outputArray.length interations)
            if(array[n] == outputArray[m]){
                duplicate = true;
            }
        }
        //if this number is not existed, add to new array
        if(duplicate == false){
            outputArray.push(array[n]);
        }
        duplicate = false;
    }
    console.log(outputArray)
    return outputArray;
}

// Logarithmic Time O(n log n)
function getAllPermutation(str) {
    var result = [];
    if (str.length < 2) return str; 

    for (let i = 0; i < str.length; i++) {
      var char = str[i];
  
      // skip if duplicate
      if (str.indexOf(char) != i) 
        continue;
  
      var remainingChars = str.slice(0, i) + str.slice(i + 1);
  
      for (var permutation of getAllPermutation(remainingChars))
        result.push(char + permutation)
    }
    
    return result;
}

//Linear Time O(N)
function comparePermutation(str1, str2){
    permutationArr = getAllPermutation(str1);
    for(let j = 0; j < permutationArr.length; j ++){
        if(str2 === permutationArr[j]){
            console.log(true)
            return true;
        }
    }
}

removeDuplicate([12, 11, 12, 21, 41, 43, 21]);
comparePermutation('RESONATE', 'ETANOSER')